package io.macaw.test.impl;

import com.cfx.message.Marshallable;
import com.cfx.message.MarshallingContext;
import io.macaw.kafka.client.api.KafkaMessageSender;
import io.macaw.kafka.client.api.KafkaSSLConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


/*
 * Helps in communication on a kafka topic...
 */
public class MessagingHelper {

    private static final String KAFKA_SERVERS_PROPERTY_KEY = "cfx.kafka.servers";

    private static final Logger logger = LoggerFactory.getLogger(MessagingHelper.class);
    private static final MessagingHelper _instance = new MessagingHelper();
    private KafkaMessageSender<String, byte[]> kafkaMessageSender;

    private MessagingHelper() {
    }

    public static MessagingHelper getInstance() {
        return _instance;
    }

    private static Properties getKafkaProducerConfigs(Properties properties) {
        final Properties kafkaProducerConfigs = new Properties();
        // Kafka SSL configs
        kafkaProducerConfigs.putAll(KafkaSSLConfigs.from(properties));
        String kafkaServers = properties.getProperty(KAFKA_SERVERS_PROPERTY_KEY);
        kafkaProducerConfigs.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServers);
        kafkaProducerConfigs.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProducerConfigs.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());

        return kafkaProducerConfigs;
    }

    public void start(Properties properties) {
        final Properties kafkaProducerConfigs = getKafkaProducerConfigs(properties);
        kafkaMessageSender = new KafkaMessageSender<>(null, kafkaProducerConfigs);
        // start the message sender
        final Future<Void> messageSenderStartResult = this.kafkaMessageSender.startAsync((runnable) -> {
            final Thread newThread = new Thread(runnable);
            newThread.setName("kafka-message-sender");
            newThread.setDaemon(true);
            return newThread;
        });
        try {
            messageSenderStartResult.get(5, TimeUnit.MINUTES);
        } catch (Exception e) {
            throw new RuntimeException("Failed to start message sender", (e instanceof ExecutionException) ? e.getCause() : e);
        }
    }

    public void stop() {
        if (kafkaMessageSender != null) {
            try {
                kafkaMessageSender.close();
            } catch (Exception e) {
                logger.debug("Ignoring exception that occurred during closing of management messaging kafka producer", e);
            }
        }
    }

    /*
     * Send message to a Kafka topic designated for service instance management
     *
     */
    public void sendMessageToMgmtTopic(final String topic, final List<Marshallable> msgs) throws IOException {
        if (msgs == null || msgs.isEmpty()) {
            return;
        }
        for (final Marshallable msg : msgs) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final DataOutputStream dataOutput = new DataOutputStream(baos);
            // write out message protocol version
            final short protocolVersion = 0x01;
            dataOutput.writeShort(protocolVersion);
            msg.marshal(new MarshallingContext(protocolVersion), dataOutput);
            dataOutput.close();
            try {
                kafkaMessageSender.send(topic, baos.toByteArray());
            } catch (Exception e) {
                throw new IOException("Failed to send messages on management topic " + topic, e);
            }
        }
        logger.info("Sent " + msgs.size() + " messages on management topic: " + topic);
    }

    public void sendMessageToMgmtTopic(final String topic, final Marshallable msg) throws Exception {
        sendMessageToMgmtTopic(topic, Collections.singletonList(msg));
    }

}
